CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Features
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Read only deployment module is intends to improve the deployment workflow. It helps your site working in read
only mode while you run a deployment and allow only certain users to access and create content in the process and locks
all other user to access only.

 * For a full description of the module, visit the project page: https://www.drupal.org/project/readonlydeployment

 * To submit bug reports and feature suggestions, or to track changes: https://www.drupal.org/project/issues/readonlydeployment



REQUIREMENTS
------------

No special requirements.


FEATURES
-------------------

 * Allow you to turn on deployment lock and make the site functioning in read only mode while deployment.
 * Add a custom warning message for all users while in read only deployment is in progress
 * Allows you to add access to certain forms to be accessed by users on the system even when in deployment mode.
 * Allows you to assign a new "deployment lock override" role to have certain users or only few administrators users to have access 
 to complete system while in maintenance mode.
 * Adds a custom permission that can be added to allow access to certain roles while in deployment.
 * Users will not be logged out when deployment lock is turned on/off and maintain functioning of system and delivering content.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

 * Configure the read only deployment lock at (/admin/config/development/maintenance).


MAINTAINERS
-----------

Current maintainers:
 * Shreyal Mandot (shreyal999) - https://www.drupal.org/u/shreyal999

