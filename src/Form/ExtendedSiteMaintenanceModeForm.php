<?php

namespace Drupal\readonlydeployment\Form;


use Drupal\system\Form\SiteMaintenanceModeForm;
use Drupal\Core\Form\FormStateInterface;

class ExtendedSiteMaintenanceModeForm extends SiteMaintenanceModeForm {

    /**
     * Add new element to the form.
     *
     * @param array $form
     *   The form object.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The form state of form.
     *
     * @return array
     *   returns the final form array.
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $site_maintenance_config = $this->config('readonlydeployment.settings');

        $form = parent::buildForm($form, $form_state);
        $form['read_only_deployment'] = [
            '#title' => t('Read Only Deployment'),
            '#type' => 'details',
            '#weight' => 1,
            '#open' => TRUE,
        ];

        $form['read_only_deployment']['read_only_deployment_lock'] = [
            '#type' => 'checkbox',
            '#title' => t('Enable Read Only Deployment'),
            '#description' => t('This will lock all content create, update and edit operations when enabled.'),
            '#weight' => 0,
            '#default_value' => $site_maintenance_config->get('read_only_deployment_lock'),
        ];

        $form['read_only_deployment']['allowed_type'] = [
          '#type' => 'checkboxes',
          '#title' => t('Allow Override for Users to view forms in readonly mode and continue editing on the basis of :'),
          '#options' => [
            'role' => t("Allow users with 'read only override' role to access system"),
            'permission' => t("Allow roles with 'read only override' permission to access system"),
          ],
          '#weight' => 0,
          '#default_value' => $site_maintenance_config->get('allowed_type'),
        ];

      $form['read_only_deployment']['allowed_forms'] = [
        '#type' => 'textarea',
        '#title' => t('Allowed Forms in Deployment lock mode'),
        '#description' => t('Add the form ids (e.x node_article_form) you want to allow while in deployment lock mode, add one form id per line.'),
        '#default_value' => $site_maintenance_config->get('allowed_forms'),
        '#rows' => 3
      ];

        $form['read_only_deployment']['default_message'] = [
            '#type' => 'textarea',
            '#title' => t('Read Only warning message'),
            '#description' => t('This warning message is displayed when viewing the site in read only deployment mode.'),
            '#default_value' => $site_maintenance_config->get('default_message'),
            '#rows' => 3,
            '#required' => TRUE,
        ];

        return $form;
    }

    /**
     * Submit handler for the form.
     *
     * @param array $form
     *   The form object.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The form state of form.
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
      $form_value = $form_state->getValues();
      \Drupal::configFactory()->getEditable('readonlydeployment.settings')
        ->set('read_only_deployment_lock', $form_value['read_only_deployment_lock'])
        ->set('allowed_type', $form_value['allowed_type'])
        ->set('default_message', $form_value['default_message'])
        ->set('allowed_forms', $form_value['allowed_forms'])
        ->save();
      parent::submitForm($form, $form_state);
    }

}